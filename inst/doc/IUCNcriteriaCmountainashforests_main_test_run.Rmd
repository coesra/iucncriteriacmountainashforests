---
title: IUCNcriteriaCmountainashforests Results of Running the Workflow
author: Ivan Hanigan and John Stein
output:
  html_document:
    toc: true
    theme: united
    number_sections: no    
  pdf_document:
    toc: true
    toc_depth: 3
    highlight: zenburn
    keep_tex: true
    number_sections: no        
documentclass: article
classoption: a4paper
csl: mee.csl
---
# Introduction
- This is an Rmarkdown workflow
- There is an associated Kepler workflow
```{r echo = F, eval=F, results="hide"}  
library(rmarkdown)
# library(knitr)
library(xtable)
rmarkdown::render("IUCNcriteriaCmountainashforests_main_test_run.Rmd", "html_document")
```
# B00_init_projectdir  
```{r echo = F, eval=T, results="hide"}
### Set the working directory
projectdir <- "~/KeplerData/workflows/MyWorkflows/IUCNcriteriaCmountainashforests"
setwd(projectdir)
indir <- projectdir
# master
#codedir <- "~/KeplerData/workflows/MyWorkflows/IUCNcriteriaCmountainashforests/code"
# or devel
codedir <- 'file.path(system.file(package="IUCNcriteriaCmountainashforests"), "doc")'
recreate_from_source <- "NO"
    
#### name:C00_init_projectdir.R ####
# convert codedir if system call from kepler string component, otherwise treat as dir path
if(grep("system.file", codedir)){
  codedir <- eval(parse(text = codedir))
}
eval <- T
if(eval){
source(file.path(codedir,"C00_init_projectdir.R"))
save.image(file.path(projectdir, ".RData"))
}
outdir <- projectdir
```

## C2_04_compute_futures_area_lost_pct_occ_within_curr_ext
```{r echo=FALSE, eval = FALSE, results='hide'}
indir_c204 <- "/home/virtual_home/public_share_data/IUCNmountainashforests_data/bioclim_biomap"
infile_biomap_current <- "mash1bm7.tif"
infile_ecosystem_current <- "data/ecosystem_current.shp"
  
#### name: C2_04_compute_futures_area_lost_pct_occ_within_curr_ext ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'C2_04_compute_futures_area_lost_pct_occ_within_curr_ext.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```

# show_results
```{r echo=FALSE, eval = T, results='asis'}
#### name: C_show_results ####
eval <- T
if(eval){ 
load(file.path(indir,'.RData'))
source(file.path(codedir,'C_show_results.R'))
save.image(file.path(indir,'.RData'))
}
outdir <- indir 
```
